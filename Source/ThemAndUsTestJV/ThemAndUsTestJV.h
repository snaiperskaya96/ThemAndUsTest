// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#ifndef __THEMANDUSTESTJV_H__
#define __THEMANDUSTESTJV_H__

#include "EngineMinimal.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Item/Item.h"

static void DisableActor(AActor* Actor)
{
	AItem* Item = Cast<AItem>(Actor);
	if (Item != nullptr) {
		Item->OnDisable();
	}

	Actor->SetActorHiddenInGame(true);
	Actor->SetActorEnableCollision(false);
	Actor->SetActorTickEnabled(false);
}

static void EnableActor(AActor* Actor)
{
	Actor->SetActorHiddenInGame(false);
	Actor->SetActorTickEnabled(true);
	Actor->SetActorEnableCollision(true);

	AItem* Item = Cast<AItem>(Actor);
	if (Item != nullptr) {
		Item->OnEnable();
	}
}

#endif
