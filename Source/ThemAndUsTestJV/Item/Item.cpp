// Fill out your copyright notice in the Description page of Project Settings.

#include "ThemAndUsTestJV.h"
#include "Item.h"


// Sets default values
AItem::AItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));

	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("InteractionArea"));
	CollisionComponent->SetSphereRadius(50.f);

	RootComponent = SkeletalMesh;
	CollisionComponent->SetupAttachment(SkeletalMesh, NAME_None);	

	AttachmentSocket = FName("RightHandSocket");

	SkeletalMesh->SetSimulatePhysics(true);
	SkeletalMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	SkeletalMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AItem::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

