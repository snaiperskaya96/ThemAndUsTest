// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Animation/BlendSpace1D.h" 
#include "Item.generated.h"

UCLASS()
class THEMANDUSTESTJV_API AItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	
	UPROPERTY(Category = Information, EditDefaultsOnly, BlueprintReadOnly)
	FName Name;

	UPROPERTY(Category = Information, EditDefaultsOnly, BlueprintReadOnly)
	FString Description;
	
	UFUNCTION(Category = Interaction, BlueprintCallable, BlueprintImplementableEvent)
	void Use();

	UFUNCTION(Category = Interaction, BlueprintImplementableEvent)
	void AfterOnHands();

	UFUNCTION(Category = Interaction, BlueprintImplementableEvent)
	void OnEnable();
	UFUNCTION(Category = Interaction, BlueprintImplementableEvent)
	void OnDisable();
protected:
	UPROPERTY(Category = Mesh, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* SkeletalMesh;	
	
	UPROPERTY(Category = Interaction, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USphereComponent* CollisionComponent;

	UPROPERTY(Category = Equipment, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FName AttachmentSocket;

	UPROPERTY(Category = Ui, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UTexture2D* ItemIcon;

public:
	UFUNCTION(BlueprintCallable, Category = Equipment)
	FORCEINLINE FName GetAttachmentSocket() const { return AttachmentSocket; }
	
	UFUNCTION(BlueprintCallable, Category = Equipment)
	FORCEINLINE UTexture2D* GetItemIcon() const { return ItemIcon; }

	UFUNCTION(BlueprintCallable, Category = Mesh)
	FORCEINLINE USkeletalMeshComponent* GetMesh() const { return SkeletalMesh; }

};
