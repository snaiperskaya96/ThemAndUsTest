// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Item/Item.h"
#include "UI/InventoryUi.h"
#include "InventoryUiItem.generated.h"

/**
 * 
 */
UCLASS()
class THEMANDUSTESTJV_API UInventoryUiItem : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(Category = Ui, BlueprintCallable)
	bool HasItem();
	UFUNCTION(Category = Ui, BlueprintCallable)
	void AssignItem(AItem* Item);
	UFUNCTION(Category = Ui, BlueprintCallable)
	void DeassignItem();
	UFUNCTION(Category = Ui, BlueprintImplementableEvent, BlueprintCallable)
	void RefreshIcon();
	virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent);

	UPROPERTY(Category = Ui, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UInventoryUi* ParentUi;
protected:
	UPROPERTY(Category = Ui, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UTexture2D* DefaultIcon;

	AItem* AssignedItem;

	UFUNCTION(BlueprintPure, Category = Ui)
	UTexture2D* GetItemIcon();

public:
	UFUNCTION(BlueprintPure, Category = Inventory)
	FORCEINLINE AItem* GetAssignedItem() const { return AssignedItem; }

};
