// Fill out your copyright notice in the Description page of Project Settings.

#include "ThemAndUsTestJV.h"
#include "MainUi.h"

void UMainUi::SetHighlightedItem_Implementation(AItem* Item)
{

}

void UMainUi::PickupConfirm_Implementation(AItem* Item)
{

}

void UMainUi::SetTypeWriterText(FText Text, UTextBlock* TextBlock) {
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);

	FTypeWriterTimer NewTimer;

	FTimerHandle TimerHandle;
	TextBlock->SetText(FText());
	NewTimer.TextBlock = TextBlock;
	NewTimer.TimerHandler = TimerHandle;

	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUFunction(
		this,
		FName("TypeWriterTimerFunction"),
		Text,
		0,
		NewTimer
	);
	TypeWriterTimers.Push(NewTimer);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, 0.05f, false);
}

void UMainUi::TypeWriterTimerFunction(FText Text, int32 Index, FTypeWriterTimer& TimerStruct)
{
	FString CurrentText = TimerStruct.TextBlock->GetText().ToString();
	FText NewText = FText::FromString(CurrentText.AppendChar(Text.ToString()[Index]));
	TimerStruct.TextBlock->SetText(NewText);
	Index++;
	GetWorld()->GetTimerManager().ClearTimer(TimerStruct.TimerHandler);
	FTimerHandle TimerHandler;
	TimerStruct.TimerHandler = TimerHandler;
	if (Index < Text.ToString().Len()) {
		FTimerDelegate TimerDelegate;
		TimerDelegate.BindUFunction(
			this,
			FName("TypeWriterTimerFunction"),
			Text,
			Index,
			TimerStruct
		);
		GetWorld()->GetTimerManager().SetTimer(TimerStruct.TimerHandler, TimerDelegate, 0.05f, false);
	}
}