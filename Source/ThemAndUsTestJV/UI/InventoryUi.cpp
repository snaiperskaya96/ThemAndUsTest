// Fill out your copyright notice in the Description page of Project Settings.

#include "ThemAndUsTestJV.h"
#include "InventoryUi.h"
#include "Character/ThemAndUsTestJVCharacter.h"

void UInventoryUi::SetPreviewItem(AItem* Item)
{
	PreviewItem = Item;
	FText StatusText = FText::FromString("Picked Up");
	AThemAndUsTestJVCharacter* Character = Cast<AThemAndUsTestJVCharacter>(GetOwningPlayer()->GetCharacter());
	if (Character && PreviewItem == Character->GetOnHandsItem()) {
		StatusText = FText::FromString("On Hands");
	}
	if (PreviewItem == nullptr) {
		StatusText = FText::FromString("");
	}
	SetStatusText(StatusText);
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	TypeWriterTimers.Empty();
	RefreshPreviewView();
}