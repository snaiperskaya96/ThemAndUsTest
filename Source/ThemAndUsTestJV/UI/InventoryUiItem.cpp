// Fill out your copyright notice in the Description page of Project Settings.

#include "ThemAndUsTestJV.h"
#include "InventoryUiItem.h"

bool UInventoryUiItem::HasItem()
{
	return AssignedItem != nullptr;
}

void UInventoryUiItem::AssignItem(AItem* Item)
{
	AssignedItem = Item;
	RefreshIcon();
}

void UInventoryUiItem::DeassignItem()
{
	AssignedItem = nullptr;
	RefreshIcon();
}

UTexture2D* UInventoryUiItem::GetItemIcon()
{
	if (AssignedItem != nullptr) {
		return AssignedItem->GetItemIcon();
	}
	return DefaultIcon;
}

FReply UInventoryUiItem::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	if (ParentUi != nullptr) {
		ParentUi->SetPreviewItem(AssignedItem);
	}
	return FReply::Handled();
}