// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Item/Item.h"
#include "MainUi.generated.h"

USTRUCT()
struct THEMANDUSTESTJV_API FTypeWriterTimer {
	GENERATED_BODY()
	FTimerHandle TimerHandler;
	UTextBlock* TextBlock;
};

/**
 * 
 */
UCLASS()
class THEMANDUSTESTJV_API UMainUi : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UFUNCTION(BlueprintCallable, Category = Ui)
	void SetTypeWriterText(FText Text, UTextBlock* TextBlock);
	TArray<FTypeWriterTimer> TypeWriterTimers;
	UFUNCTION()
	void TypeWriterTimerFunction(FText Text, int32 Index, FTypeWriterTimer& TimerStruct);

public:
	UFUNCTION(BlueprintNativeEvent)
    void SetHighlightedItem(AItem* Item);

	UFUNCTION(BlueprintNativeEvent)
	void PickupConfirm(AItem* Item);
};
