// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Item/Item.h"
#include "UI/MainUi.h"
#include "InventoryUi.generated.h"

/**
 * 
 */
UCLASS()
class THEMANDUSTESTJV_API UInventoryUi : public UMainUi
{
	GENERATED_BODY()
	
public:
	/*
	* Returns true if sliding in,
	* false if sliding out
	*/
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Ui)
	bool ToggleSlide();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Ui)
	void RefreshInventory(const TArray<AItem*>& Inventory);
	
	UFUNCTION(BlueprintCallable, Category = Ui)
	void SetPreviewItem(AItem* Item);

	UFUNCTION(BlueprintImplementableEvent)
	void RefreshPreviewView();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Ui)
	void SetStatusText(const FText& Text);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Ui)
	void OpenItemMenuAtLocation(AItem* Item, FVector2D Location);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Ui)
	void OpenPreview();
protected:
	UPROPERTY(BlueprintReadOnly, Category = Ui)
	AItem* PreviewItem;
};
