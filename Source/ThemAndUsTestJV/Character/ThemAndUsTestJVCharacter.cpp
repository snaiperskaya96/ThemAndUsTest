// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "ThemAndUsTestJV.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "ThemAndUsTestJVCharacter.h"

//////////////////////////////////////////////////////////////////////////
// AThemAndUsTestJVCharacter

AThemAndUsTestJVCharacter::AThemAndUsTestJVCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AThemAndUsTestJVCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AThemAndUsTestJVCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AThemAndUsTestJVCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &AThemAndUsTestJVCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AThemAndUsTestJVCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &AThemAndUsTestJVCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AThemAndUsTestJVCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AThemAndUsTestJVCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AThemAndUsTestJVCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AThemAndUsTestJVCharacter::OnResetVR);

	PlayerInputComponent->BindAction("Use", IE_Pressed, this, &AThemAndUsTestJVCharacter::OnUse);
	PlayerInputComponent->BindAction("UseOnHandsItem", IE_Pressed, this, &AThemAndUsTestJVCharacter::OnUseOnHandsItem);
	PlayerInputComponent->BindAction("OpenInventory", IE_Pressed, this, &AThemAndUsTestJVCharacter::OnOpenInventory);
}

void AThemAndUsTestJVCharacter::AddControllerPitchInput(float Val)
{
	if (IsInteracting) {
		return;
	}

	if (Val != 0.f && Controller && Controller->IsLocalPlayerController()) {
		APlayerController* const PC = CastChecked<APlayerController>(Controller);
		PC->AddPitchInput(Val);
	}
}

void AThemAndUsTestJVCharacter::AddControllerYawInput(float Val)
{
	if (IsInteracting) {
		return;
	}

	if (Val != 0.f && Controller && Controller->IsLocalPlayerController()) {
		APlayerController* const PC = CastChecked<APlayerController>(Controller);
		PC->AddYawInput(Val);
	}
}

void AThemAndUsTestJVCharacter::StartInteracting()
{
	IsInteracting = true;

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();

	PlayerController->bShowMouseCursor = true;
	PlayerController->bEnableClickEvents = true;
	PlayerController->bEnableMouseOverEvents = true;
}

void AThemAndUsTestJVCharacter::StopInteracting()
{
	IsInteracting = false;

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();

	PlayerController->bShowMouseCursor = false;
	PlayerController->bEnableClickEvents = false;
	PlayerController->bEnableMouseOverEvents = false;
}

void AThemAndUsTestJVCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FHitResult HitResult;
	FCollisionObjectQueryParams LineTraceObjectQueryParams(ECC_WorldDynamic);
	FCollisionQueryParams CollisionParams;

	CollisionParams.AddIgnoredActor(OnHandsItem);

	/** Debug	
	const FName TraceTag("TraceTag");
	GetWorld()->DebugDrawTraceTag = TraceTag;
	CollisionParams.TraceTag = TraceTag;
	**/

	APlayerController* PC = Cast<APlayerController>(Controller);
	if (PC == nullptr) {
		return;
	}

	FVector CameraLocation = PC->PlayerCameraManager->GetCameraLocation();
	FVector MyLocation = GetActorLocation();
	FVector Direction = MyLocation - CameraLocation;
	Direction.Normalize();

	if (GetWorld()->LineTraceSingleByObjectType(
		HitResult,
		MyLocation + FVector(0.f, 0.f, BaseEyeHeight / 2),
		CameraLocation + Direction * 1000,
		LineTraceObjectQueryParams,
		CollisionParams
	)) {
		AItem* Item = Cast<AItem>(HitResult.Actor.Get());
		if (MainUiInstance != nullptr) {
			MainUiInstance->SetHighlightedItem(Item);
			HighlightedItem = Item;
		}
	} else {
		MainUiInstance->SetHighlightedItem(nullptr);
	}

}

void AThemAndUsTestJVCharacter::BeginPlay()
{
	Super::BeginPlay();
	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	if (PlayerController != nullptr) {
		if (MainUi != nullptr && MainUiInstance == nullptr) {
			MainUiInstance = CreateWidget<UMainUi>(PlayerController, MainUi);
			if (MainUiInstance != nullptr) {
				MainUiInstance->AddToViewport();
				MainUiInstance->SetVisibility(ESlateVisibility::Visible);
			}
		}

		if (InventoryUi != nullptr && InventoryUiInstance == nullptr) {
			InventoryUiInstance = CreateWidget<UInventoryUi>(PlayerController, InventoryUi);
			if (InventoryUiInstance != nullptr) {
				InventoryUiInstance->AddToViewport();
				InventoryUiInstance->SetVisibility(ESlateVisibility::Hidden);
			}
		}
	}

}

void AThemAndUsTestJVCharacter::OnOpenInventory()
{
	if (InventoryUiInstance == nullptr) {
		return;
	}

	if (InventoryUiInstance->ToggleSlide()) {
		StartInteracting();
	} else {
		StopInteracting();
	}
}

void AThemAndUsTestJVCharacter::OnUse()
{
	if (IsInteracting) {
		return;
	}

	if (HighlightedItem == nullptr
		|| Inventory.Find(HighlightedItem) != INDEX_NONE
		|| OnHandsItem == HighlightedItem) {
		return;
	}

	PickupItem(HighlightedItem);
}

void AThemAndUsTestJVCharacter::PickupItem(AItem * Item)
{
	if (OnHandsItem != Item 
		&& Inventory.Find(Item) == INDEX_NONE
		&& Inventory.Num() < 6
	) {
		StartInteracting();
		MainUiInstance->PickupConfirm(Item);
	}
}

void AThemAndUsTestJVCharacter::PutInInventory(AItem * Item)
{
	if (Item == nullptr
		|| Inventory.Find(Item) != INDEX_NONE
		|| Inventory.Num() >= 6
	){
		return;
	}

	Item->GetMesh()->SetSimulatePhysics(false);
	Item->GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Item->GetMesh()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);

	Item->SetOwner(this);
	DisableActor(Item);
	Inventory.Push(Item);
	
	if (InventoryUiInstance != nullptr) {
		InventoryUiInstance->RefreshInventory(Inventory);
	}
}

void AThemAndUsTestJVCharacter::RemoveItemFromInventory(AItem* Item)
{
	if (Inventory.Find(Item) == INDEX_NONE) {
		return;
	}

	if (OnHandsItem == Item) {
		OnHandsItem->DetachRootComponentFromParent();
		OnHandsItem = nullptr;
		GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	}

	Inventory.RemoveAt(Inventory.Find(Item));
	InventoryUiInstance->RefreshInventory(Inventory);
}

void AThemAndUsTestJVCharacter::UseItem(AItem* Item)
{
	if (Inventory.Find(Item) == INDEX_NONE) {
		return;
	}

	IsHoldingTorch = false;

	if (OnHandsItem != nullptr) {
		OnHandsItem->DetachRootComponentFromParent();
		DisableActor(OnHandsItem);
		GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
		if (OnHandsItem == Item) {
			OnHandsItem = nullptr;
			// Toggling hands <-> inventory
			return;
		}
		OnHandsItem = nullptr;
	}

	OnHandsItem = Item;
	EnableActor(Item);
	Item->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, Item->GetAttachmentSocket());
	Item->AfterOnHands();
}

void AThemAndUsTestJVCharacter::OnUseOnHandsItem()
{
	if (OnHandsItem != nullptr) {
		OnHandsItem->Use();
	}
}

void AThemAndUsTestJVCharacter::DiscardItem(AItem* Item)
{
	if (Inventory.Find(Item) == INDEX_NONE) {
		return;
	}

	if (Item == OnHandsItem) {
		GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
		OnHandsItem->DetachRootComponentFromParent();
		OnHandsItem = nullptr;
	}

	Item->SetActorLocation(GetActorLocation() + (GetActorRotation().Vector() + FVector::ForwardVector * 30.0f));
	EnableActor(Item);
	Item->GetMesh()->SetSimulatePhysics(true);
	Item->GetMesh()->SetCollisionProfileName("BlockAll");

	Inventory.RemoveAt(Inventory.Find(Item));
	InventoryUiInstance->RefreshInventory(Inventory);
}

TArray<class AItem*> AThemAndUsTestJVCharacter::GetInventory()
{
	return Inventory;
}

void AThemAndUsTestJVCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AThemAndUsTestJVCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AThemAndUsTestJVCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void AThemAndUsTestJVCharacter::TurnAtRate(float Rate)
{
	if (IsInteracting) {
		return;
	}

	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AThemAndUsTestJVCharacter::LookUpAtRate(float Rate)
{
	if (IsInteracting) {
		return;
	}

	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AThemAndUsTestJVCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AThemAndUsTestJVCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
