// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "UI/MainUi.h"
#include "UI/InventoryUi.h"
#include "ThemAndUsTestJVCharacter.generated.h"

UCLASS(config=Game)
class AThemAndUsTestJVCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AThemAndUsTestJVCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UFUNCTION(BlueprintCallable, Category = Inventory)
	TArray<class AItem*> GetInventory();

	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void PutInInventory(AItem* Item);

	UFUNCTION(BlueprintCallable, Category = Interaction)
	void UseItem(AItem* Item);

	UFUNCTION(BlueprintCallable, Category = Interaction)
	void RemoveItemFromInventory(AItem* Item);

	UFUNCTION(BlueprintCallable, Category = Interaction)
	void DiscardItem(AItem* Item);

	UPROPERTY(Category = Interaction, BlueprintReadWrite, VisibleAnywhere)
	bool IsHoldingTorch;

protected:
	void AddControllerYawInput(float Val);

	void AddControllerPitchInput(float Val);

	bool IsInteracting = false;
	/** Resets HMD orientation in VR. */
	void OnResetVR();

	void OnUse();

	void OnOpenInventory();

	void PickupItem(AItem* Item);

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	void OnUseOnHandsItem();

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ui, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UMainUi> MainUi;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ui, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UInventoryUi> InventoryUi;

	UMainUi* MainUiInstance;
	UInventoryUi* InventoryUiInstance;
	AItem* HighlightedItem;
	// End of APawn interface

	AItem* OnHandsItem;
	TArray <AItem*> Inventory;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UFUNCTION(BlueprintCallable, Category = Inventory)
	FORCEINLINE AItem* GetOnHandsItem() const { return OnHandsItem; }
	
	UFUNCTION(BlueprintCallable, Category = Interaction)
	void StartInteracting();

	UFUNCTION(BlueprintCallable, Category = Interaction)
	void StopInteracting();
};

